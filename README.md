# [chmod-permissions](https://gitlab.com/eidoom/chmod-permissions)

Convert between numeric and text representations of [Linux file permissions](https://wiki.archlinux.org/title/File_permissions_and_attributes).

Note: doesn't handle `-` as first character of argument.
