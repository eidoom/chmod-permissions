#!/usr/bin/env python3

import argparse, re

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Convert between numeric and text representations of Linux file permissions",
    )
    parser.add_argument(
        "permissions",
        type=str,
        help="in numeric (750) or text (rwxr-x---) representation",
    )
    ps = parser.parse_args().permissions

    if re.fullmatch(r"[0-7]{3}", ps):
        print(
            "".join(
                (
                    "".join(
                        [
                            l if i == "1" else "-"
                            for i, l in zip(f"{int(n):03b}", ("r", "w", "x"))
                        ]
                    )
                )
                for n in ps
            )
        )

    elif re.fullmatch(r"[rwx\-]{9}", ps):
        print(
            "".join(
                [
                    str(int(b.translate(str.maketrans("rwx-", "1110")), 2))
                    for b in [ps[3 * i : 3 * (i + 1)] for i in range(3)]
                ]
            )
        )

    else:
        print(f"Didn't recognise input {ps}")
